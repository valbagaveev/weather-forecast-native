import React, {useState} from 'react'
import {StyleSheet, View} from 'react-native';
import ErrorChoiceNotif from '../common/components/ErrorChoiceNotif'
import CalcFutForecast from './CalcFutForecast'
import SelectCity from '../common/components/SelectCity'

const FutureForecast = () => {
    const [city, setCity] = useState('')

    const visibleWeather = (!city) ?
        <ErrorChoiceNotif text="Fill in all the fields and the weather will be displayed"/> :
        <CalcFutForecast city={city}/>

    const onCitySelected = (name) => {
        setCity(name)
    }

    return (
        <View style={styles.futureForecast}>
            <SelectCity OnCitySelected={onCitySelected}/>
            {visibleWeather}
        </View>
    )
}

export default FutureForecast

const styles = StyleSheet.create({
    futureForecast: {
        flexDirection: 'column',
        alignItems: 'center'
    },
})