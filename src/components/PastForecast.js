import React, { useState } from 'react'
import ErrorChoiceNotif from '../common/components/ErrorChoiceNotif'
import SelectCity from '../common/components/SelectCity'
import {StyleSheet, View} from "react-native";
import CalcPastForecast from "./CalcPastForecast";
import InputDate from "../common/components/InputDate";

const PastForecast = () => {
    const [city, setCity] = useState('')

    const [date, setDate] = useState(new Date().getTime())

    const visibleWeather = (!city || !date) ? <ErrorChoiceNotif text="Fill in all the fields and the weather will be displayed"/> : <CalcPastForecast city={city} date={date}/>

    const onCitySelected = (name) => {
        setCity(name)
    }

    const onDateSelected = (item) => {
        setDate(item)
    }

    return (
        <View style={styles.pastForecast}>
            <View style={styles.pastForecast__choice}>
                <SelectCity OnCitySelected={onCitySelected}/>
                <InputDate OnDateSelected={onDateSelected}/>
            </View>
            {visibleWeather}
        </View>
    )
}

export default PastForecast

const styles = StyleSheet.create({
    pastForecast: {
        flexDirection: 'column',
        alignItems: 'center'
    },

    pastForecast__choice: {
        flexDirection: 'column',
        width: '100%',
    },
})
