import React, {useState, useEffect} from 'react'
import OpenWeather from '../services/open-weather'
import {COORDINATES} from '../common/store/data'
import VisualCard from '../common/components/VisualCard'
import {StyleSheet, SafeAreaView, ScrollView} from "react-native";


const CalcFutForecast = (props) => {
    const [forecastAnswer, setForecastAnswer] = useState([])

    const openWeather = new OpenWeather()

    let lat = ''
    let lon = ''

    for (const [key, values] of Object.entries(COORDINATES)) {
        if (key === props.city) {
            lat = values[0]
            lon = values[1]
        }
    }

    useEffect(() => {
        openWeather.getWeather7Days(lat, lon)
            .then((forecast) => {
                setForecastAnswer(forecast)
            })
    }, [lat, lon])


    const renderItems = (arr) => {
        return arr.map((el, key) => {
            return (
                <VisualCard key={key} day={el.day} month={el.month} year={el.year} tmp={el.tmp} icon={el.icon}
                            frame="future"/>
            )
        })
    }

    const items = renderItems(forecastAnswer)

    return (
        <SafeAreaView style={styles.calcForecastTemplate__items}>
            <ScrollView horizontal={true}>
                {items}
            </ScrollView>
        </SafeAreaView>
    )
}

export default CalcFutForecast

const styles = StyleSheet.create({
    calcForecastTemplate__items: {
        flexDirection: 'row',
        marginTop: 30,
        width: '100%'
    },
})