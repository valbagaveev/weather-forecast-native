import React, { useState, useEffect } from 'react'
import OpenWeather from '../services/open-weather'
import { COORDINATES } from '../common/store/data'
import VisualCard from '../common/components/VisualCard'
import ErrorChoiceNotif from '../common/components/ErrorChoiceNotif'
import {StyleSheet, View} from "react-native"

const CalcPastForecast = (props) => {
    const [forecastAnswer, setForecastAnswer] = useState({
        day: '',
        month: '',
        year: '',
        tmp: 0,
        icon: '',
        cod: '',
        message: ''
    })

    const openWeather = new OpenWeather()

    let lat = ''
    let lon = ''

    for (const [key, values] of Object.entries(COORDINATES)) {
        if (key === props.city) {
            lat = values[0]
            lon = values[1]
        }
    }

    useEffect(() => {
        openWeather.getWeatherPrevious(lat, lon, props.date)
            .then((forecast) => {
                setForecastAnswer(forecast)
            })
    }, [lat, lon, props.date])

    const showComponent = (forecastAnswer.day && forecastAnswer.month && forecastAnswer.year && forecastAnswer.tmp && forecastAnswer.icon)
        ? <VisualCard day={forecastAnswer.day} month={forecastAnswer.month} year={forecastAnswer.year}
                      tmp={forecastAnswer.tmp} icon={forecastAnswer.icon} frame="past"/>
        : (forecastAnswer.cod && forecastAnswer.message) ? <ErrorChoiceNotif text={forecastAnswer.message[0].toUpperCase() + forecastAnswer.message.slice(1)}/> : null

    return (
        <View style={styles.calcPastForecastTemplate}>
            {showComponent}
        </View>
    )
}

export default CalcPastForecast

const styles = StyleSheet.create({
    calcPastForecastTemplate: {
        width: '100%'
    },
})