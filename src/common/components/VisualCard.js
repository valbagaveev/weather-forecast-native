import React from 'react'
import {StyleSheet, View, Text, Image} from 'react-native';


const VisualCard = (props) => {
    const currentTemp = (props.tmp > 0) ? `+${props.tmp}` : (props.tmp < 0) ? `-${props.tmp}` : `${props.tmp}`

    return (
        <View style={props.frame==="future" ? styles.visualCardTemplate__future : styles.visualCardTemplate__past}>
            <Text style={styles.visualCardTemplate__date}>
                {props.day} {props.month} {props.year}
            </Text>
            <View style={styles.visualCardTemplate__icon}>
                <Image style={{width: 100, height: 100}} source={{
                    uri: `${props.icon}`,
                }} />
            </View>
            <Text style={styles.visualCardTemplate__number}>
                {currentTemp}&deg;
            </Text>
        </View>
    )
}

export default VisualCard

const styles = StyleSheet.create({
    visualCardTemplate__future: {
        backgroundColor: '#373AF5',
        padding: 20,
        width: 174,
        flexDirection: 'column',
        alignItems: 'center',
        borderRadius: 8,
        marginRight: 16
    },

    visualCardTemplate__past: {
        width: '100%',
        backgroundColor: '#373AF5',
        padding: 20,
        flexDirection: 'column',
        alignItems: 'center',
        borderRadius: 8,
    },

    visualCardTemplate__date: {
        alignSelf: 'flex-start',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 16,
        lineHeight: 24,
        color: '#fff'
    },

    visualCardTemplate__icon: {
        marginTop: 15,
        marginBottom: 7,
        marginHorizontal: 7
    },

    visualCardTemplate__number: {
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 32,
        lineHeight: 32,
        color: '#fff',
        alignSelf: 'flex-end'
    }
})