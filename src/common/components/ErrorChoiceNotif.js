import React from 'react'
import {StyleSheet, View, Text, Image} from 'react-native';

const ErrorChoiceNotif = (props) => {
    return (
        <View style={styles.errorChoiceNotif}>
            <Image source={require('./images/cloud.png')} />
            <Text style={styles.errorChoiceNotif__text}>{props.text}</Text>
        </View>
    )
}

export default ErrorChoiceNotif

const styles = StyleSheet.create({
    errorChoiceNotif: {
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 30
    },

    errorChoiceNotif__text: {
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 24,
        marginTop: 24,
        color: '#8083A4',
        textAlign: 'center',
    },
})
