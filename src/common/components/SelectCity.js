import React, { useState } from 'react'
import { COORDINATES } from '../store/data'
import RNPickerSelect from 'react-native-picker-select';
import { Chevron } from 'react-native-shapes';

const SelectCity = (props) => {
    const [value, setValue] = useState('')

    const renderItems = (data) => {
        return Object.keys(data).map((city) => {
            return(
                {label: city[0].toUpperCase() + city.slice(1), value: city}
            )
        })
    }

    const handleChange = (value) => {
        setValue(value)
        return value
    }

    const itemsCities = renderItems(COORDINATES)

    return (
        <RNPickerSelect
            placeholder={{
                label: 'Select city',
                value: null,
            }}
            style={{
                placeholder: {
                    color: '#8083a4',
                    fontSize: 16,
                },

                inputIOS: {
                    fontSize: 16,
                    paddingVertical: 10,
                    paddingHorizontal: 16,
                    borderWidth: 2,
                    borderColor: 'rgba(128,131,164,.2)',
                    borderRadius: 8,
                    color: '#2c2d76',
                    paddingRight: 30, // to ensure the text is never behind the icon,
                    backgroundColor: 'rgba(128,131,164,.06)'
                },

                inputAndroid: {
                    fontSize: 16,
                    paddingVertical: 10,
                    paddingHorizontal: 16,
                    borderWidth: 2,
                    borderColor: 'rgba(128,131,164,.2)',
                    borderRadius: 8,
                    color: '#2c2d76',
                    paddingRight: 30, // to ensure the text is never behind the icon
                    backgroundColor: 'rgba(128,131,164,.06)'
                },

                iconContainer: {
                    top: 17,
                    right: 20,
                },

            }}
            Icon={() => {
                return <Chevron size={1.5} color="gray" />;
            }}
            onValueChange={(value) => props.OnCitySelected(handleChange(value))}
            items={[
                ...itemsCities
            ]}
        />
    )
}
export default SelectCity
