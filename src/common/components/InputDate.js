import React, {useState} from 'react'
import DateTimePicker from '@react-native-community/datetimepicker';
import {StyleSheet} from "react-native";

const InputDate = (props) => {
    const [date, setDate] = useState(new Date());

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setDate(currentDate);
        props.OnDateSelected(currentDate.getTime())
    };

    return (
        <DateTimePicker
            testID="dateTimePicker"
            value={date}
            display="inline"
            onChange={onChange}
            style={styles.datepicker}
        />
    )
}
export default InputDate

const styles = StyleSheet.create({
    datepicker: {
        width: '100%',
        marginTop: 20,
        alignSelf: 'center',
    },
})