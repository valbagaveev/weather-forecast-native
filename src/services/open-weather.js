export default class OpenWeather {
    _apiBase = 'https://api.openweathermap.org/data/2.5';
    _apiKey = 'c8a8fb5580fdb166e938845912ca6d28';
    _imageBase = 'https://openweathermap.org/img/wn/'

    async getResource (url) {
        const res = await fetch(`${this._apiBase}${url}`)
        return await res.json()
    }

    async getWeather7Days (lat, lon) {
        const weather = await this.getResource(`/onecall?lat=${lat}&lon=${lon}&units=metric&exclude=hourly,minutely&appid=${this._apiKey}`)
        return this._transformWeather7Days(weather)
    }

    async getWeatherPrevious (lat, lon, time) {
        const weather = await this.getResource(`/onecall/timemachine?lat=${lat}&lon=${lon}&units=metric&dt=${(time / 1000).toFixed(0)}&appid=${this._apiKey}`)
        if (weather.cod && weather.message) {
            return weather
        }

        return this._transformWeatherPreviousDay(weather)
    }

    fromDtToString (elem) {
        const milliseconds = elem.dt * 1000
        return new Date(milliseconds)
    }

    _getData7Days (item) {
        const dailyTemp = item.daily.slice(1).map(
            (el) => {
                return {
                    day: this.fromDtToString(el).toLocaleString('en-US', { day: 'numeric' }),
                    month: this.fromDtToString(el).toLocaleString('en-US', { month: 'long' }),
                    year: this.fromDtToString(el).toLocaleString('en-US', { year: 'numeric' }),
                    tmp: Math.round(el.temp.max),
                    icon: `${this._imageBase}${el.weather[0].icon}@2x.png`
                }
            }
        )

        const currentTemp = {
            day: this.fromDtToString(item.current).toLocaleString('en-US', { day: 'numeric' }),
            month: this.fromDtToString(item.current).toLocaleString('en-US', { month: 'long' }),
            year: this.fromDtToString(item.current).toLocaleString('en-US', { year: 'numeric' }),
            tmp: Math.round(item.current.temp),
            icon: `${this._imageBase}${item.current.weather[0].icon}@2x.png`
        }

        dailyTemp.splice(0, 0, currentTemp)

        return dailyTemp
    }

    _getPreviousDay (item) {
        return {
            day: this.fromDtToString(item.current).toLocaleString('en-US', { day: 'numeric' }),
            month: this.fromDtToString(item.current).toLocaleString('en-US', { month: 'long' }),
            year: this.fromDtToString(item.current).toLocaleString('en-US', { year: 'numeric' }),
            tmp: Math.round(item.current.temp),
            icon: `${this._imageBase}${item.current.weather[0].icon}@2x.png`
        }
    }

    _transformWeather7Days = (weather) => {
        return this._getData7Days(weather)
    }

    _transformWeatherPreviousDay = (weather) => {
        return this._getPreviousDay(weather)
    }
}
