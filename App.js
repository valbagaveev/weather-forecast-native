import React, {useCallback} from 'react';
import {StyleSheet, View, Text, Pressable, Linking, Alert, SafeAreaView, ScrollView} from 'react-native';
import FutureForecast from "./src/components/FutureForecast";
import PastForecast from "./src/components/PastForecast";

export default function App() {

    const mercURL = "https://mercdev.ru/";

    const OpenURLButton = ({url, children}) => {
        const handlePress = useCallback(async () => {
            const supported = await Linking.canOpenURL(url);

            if (supported) {
                await Linking.openURL(url);
            } else {
                Alert.alert(`Don't know how to open this URL: ${url}`);
            }
        }, [url]);

        return <Pressable style={styles.weatherForecast__button} onPress={handlePress}>
            <Text style={styles.weatherForecast__text}>{children}</Text>
        </Pressable>
    };

    return (
        <SafeAreaView style={styles.weatherForecast}>
            <ScrollView style={styles.weatherForecast__view}>
                <View style={styles.weatherForecast__title}>
                    <Text style={styles.weatherForecast__title__top}>Weather</Text>
                    <Text style={styles.weatherForecast__title__bottom}>forecast</Text>
                </View>
                <View style={styles.weatherForecast__content}>
                    <View style={styles.weatherForecast__content__block}>
                        <Text style={styles.weatherForecast__content__block__subtitle}>7 Days Forecast</Text>
                        <FutureForecast/>
                    </View>
                    <View style={styles.weatherForecast__content__block}>
                        <Text style={styles.weatherForecast__content__block__subtitle}>Forecast for a Date in the
                            Past</Text>
                        <PastForecast/>
                    </View>
                </View>
                <View style={styles.weatherForecast__bottom}>
                    <OpenURLButton url={mercURL}>C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT</OpenURLButton>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    weatherForecast__button: {
        height: 30,
        width: '100%',
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 20
    },

    weatherForecast__text: {
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        lineHeight: 18,
        color: 'hsla(0,0%,100%,.6)'
    },

    weatherForecast: {
        flex: 1,
        backgroundColor: '#373AF5',
    },

    weatherForecast__view: {
        backgroundColor: '#373AF5',
        flexDirection: 'column',
        paddingTop: 14,
        paddingHorizontal: 10,
    },

    weatherForecast__title: {
        display: 'flex',
        flexDirection: 'row',
        alignSelf: 'center',
    },

    weatherForecast__title__top: {
        fontSize: 32,
        lineHeight: 32,
        color: '#FFFFFF',
        fontWeight: 'bold',
    },

    weatherForecast__title__bottom: {
        fontSize: 32,
        lineHeight: 32,
        color: '#FFFFFF',
        fontWeight: 'bold',
        marginTop: 30,
        marginLeft: -15
    },

    weatherForecast__content: {
        marginTop: 24,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        marginBottom: 20
    },

    weatherForecast__content__block: {
        backgroundColor: '#fff',
        borderRadius: 8,
        paddingTop: 32,
        paddingHorizontal: 24,
        paddingBottom: 60,
        width: '100%',
        marginBottom: 34
    },

    weatherForecast__content__block__subtitle: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 32,
        lineHeight: 32,
        color: '#2c2d76',
        marginBottom: 32,
    },

    weatherForecast__bottom: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 14,
        lineHeight: 18,
        color: 'hsla(0,0%,100%,.6)',
    },

    weatherForecast__bottom__link: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 14,
        lineHeight: 18,
        color: 'hsla(0,0%,100%,.6)',
        width: 2,
    },

});
